# **Cliente – Mensagens**

  

## 1 Conectar ao Servidor

1.1 Login

  

    tetrifaster TestUser 1.13

  

  

1.2 Nome da Equipe

  

    team <number> <team name>

  

  

1.3 Informacão do Cliente

  

    clientinfo <client name> <client version>

  

  

## 2 Comunicação do Cliente

  

2.1 Mensagem de bate-papo da partida

  

    pline <sendernum> <message>

  

  

2.2 Acão de bate-papo da Partida

  

    plineact <sendernum> <action>

  

  

2.3 Mensagem de Bate-Papo do Jogo

  

    gmsg <message>

  

  

## 3 O Jogo

  

3.1 Iniciar e Parar Jogo

  

    gmsg <message>

  

  

3.2 Atualizacão de Campo

  

    f <playernum> <fieldstring>

  

  

3.3 Atualizacão de nível

  

    lvl <playernum> <levelnum>

  

  

3.4 Enviar Especial

  

    sb <targetnum> <specialtype> <sendernum>

  

  

3.5 Enviar Linhas de Adicão de Estilo Clássico

  

    sb 0 cs<numlines> <sendernum>

  

  

3.6 Jogador Eliminado

  

    playerlost <playernum>

  

  

3.7 Pausar e Retomar Jogo

  

    pause <state> <playernum>