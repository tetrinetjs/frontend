
# Interpretação de Strings

## Iniciando tabuleiro

```javascript
startBoard() {
return  "0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000005000000000055000000000050000000000000000000000000000000000000004220a0020344422011222333111101";
}
```
## Receber Tabulerio

* Tabuleiro do jogador 2
```
msg: f 2 g654371259211158514612r8q52458287696712r247422638619r198g1
67689512855157671q41q735235918758466666286393o69485932649926no79q33
2o2a6q3634952q9q32c66o2395gc5298rq73r4348797321241b813493o2c35b5594
48722a1271co4557a5136n964ob9348363468226544n5ro44634896539723497925
c6634
```
## Recebendo Tabulerios
```javascript
generateTetrisPanel() {
let  rows  = [];
for (let  i  =  1; i  <=  dimension.MAX_PLAYERS; i++) {
rows.push(
<DivBoardStyle  key={i}>
<TetrisPanelComponent  id={i}  player={this.state.player.id === i}  />
</DivBoardStyle>
);}
```
## Renderizando tabuleiros

* Enviando tabuleiro a ser renderizado
```javascript
render() {
return (
<div>
{!this.state.player ? (
<LoginComponent  />
) : (
<DivBoardTopStyle>
<Tabs  defaultActiveKey="GAME"  transition={false}  id="tabs">
<Tab  eventKey="GAME"  title="Jogo">
<div>
<OptionsComponent  player={this.state.player.id}  />
</div>
```
```
<DivPanelStyle>{this.generateTetrisPanel()}</DivPanelStyle>
```
```javascript
</Tab>
<Tab  eventKey="CHAT"  title="Chat">
<ChatComponent  player={this.state.player.id}  />
</Tab>
<Tab  eventKey="RANKING"  title="Ranking">
<RankingComponent  />
</Tab>
</Tabs>
</DivBoardTopStyle>
)}
</div>
);
}
```

## Renderizando tabulerio definido
* gamePanel é o tabuleiro completo

```javascript
render() {
return (
<div>
{this.state.tetrisMain ? (
<DivTetriMainStyle>
<span>
<b>{this.state.idComponent}</b> - {this.state.player.nickname}{" "}
{this.state.player.team ? (
<b>{"(" + this.state.player.team + ")"}</b>
) : (
""
)}
</span>
<PieceQueueComponent  player={this.state.idComponent}  />
<table>
<tbody>{this.state.gamePanel}</tbody>
</table>
<SpecialQueueComponent  player={this.state.idComponent}  />
</DivTetriMainStyle>
) : (
<DivTetriStyle>
<SpanStyle>
<b>{this.state.idComponent}</b> - {this.state.player.nickname}{" "}
{this.state.player.team ? (
<b>{"(" + this.state.player.team + ")"}</b>
) : (
""
)}
</SpanStyle>
<table>
<tbody>{this.state.gamePanel}</tbody>
</table>
</DivTetriStyle>
)}
</div>
);
}
```

## Convertendo Strings
```javascript
converterString(string) {
const  gamePanel  = [];
const  tetrisDimesion  =  this.state.tetrisMain
?  "playerDimension"
:  "otherPlayersDimension";
for (var  i  =  0; i  <  string.length; i  +=  dimension.GAME_HEIGHT) {
const  row  =  string.substr(i, dimension.GAME_HEIGHT);
const  rowArray  = [];
for (var  y  =  0; y  <  dimension.GAME_HEIGHT; y++) {
const  classString  =  `${tetrisDimesion} game-block piece-${row.charAt(
y
)}`;
rowArray.push(<td  key={y}  className={classString}  />);
}
gamePanel.push(<tr  key={i}>{rowArray}</tr>);
}
this.setState({ gamePanel });
}
```

## Atualizando tabuleiro
* Atualizando
```javascript
updateBoard(board) {
this.setState({ board }, () => {
this.converterString(this.state.board);
});
}
```
* Atualizando pacialmente
```javascript
updatePartialBoard(partialBoard) {
const  piece  =  UTILS.extractPiece(partialBoard);
const  newBoard  =  UTILS.setPartialBoard(
this.state.board,
partialBoard.substr(1),
piece
);
this.setState({ board:  newBoard }, () => {
this.converterString(this.state.board);
});
}
```
## Renderizando Parcialmente
```javascript
setPartialBoard(board, partialBoard, piece) {
let  newBoard  =  _.cloneDeep(board);
let  partialBoardConverter  =  _.cloneDeep(_.toUpper(_.trim(partialBoard)));
for (var  i  =  0; i  <  partialBoardConverter.length; i  +=  2){
var  coordinate  =
(partialBoardConverter[i  +  1].charCodeAt(0) -  51) *
dimension.GAME_HEIGHT  +
partialBoardConverter[i].charCodeAt(0) -  51);
newBoard  =
newBoard.substring(0, coordinate) +
piece + 
newBoard.substring(coordinate  +  1);
}
return  newBoard;
}
```

