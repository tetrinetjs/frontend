**Dependências usada pelo cliente**	

|   Dependência  | Descrição/Necessidade|
|-------------------|-------------------|
|Bootstrap: 4.3.1| Usado para executar o nmp, compilar CSS, JavaScript, e etc.
|Events: 3.0.0| Implementa o módulo de eventos Node.js para ambientes que não o possuem, como navegadores.
|Flux: 3.1.3| Uma arquitetura de aplicativo para o React utilizando um fluxo de dados unidirecional. https://www.npmjs.com/package/flux
|Keymaster: 1.6.2|É uma micro-biblioteca simples para definir e despachar atalhos de teclado em aplicativos da web. Não tem dependências. https://libraries.io/npm/keymaster 
|Loadsh:0.0.4|O Lodash torna o JavaScript mais fácil, eliminando o incômodo de trabalhar com matrizes, números, objetos, cadeias etc.Os métodos modulares do Lodash são ótimos para: matrizes, objetos, strings, cria funções compostas, e etc.
|Lodash: 4.17.11|A biblioteca Lodash exportada como módulos Node.js. Não tem dependências.
|Node-fetch: 2.6.0| Disponibiliza: fetch, http, promise 
|React: 16.8.6|React é uma biblioteca JavaScript para criar interfaces com o usuário.
|React-bootstrap: 1.0.0-beta.9| Disponibiliza: react, ecosystem-react, react-component, bootstrap
|React-dom: 16.8.6|Este pacote serve como ponto de entrada para os representantes do DOM e do servidor para o React. Destina-se a ser emparelhado com o pacote genérico React, que é enviado como reagir ao npm.
|React-scripts: 3.0.1|Este pacote inclui scripts e configurações usados pelo Create React App.
|Styled-components: 4.2.0|Utilizando literais de modelo marcados (uma adição recente ao JavaScript) e o poder do CSS, os componentes de estilo permitem que você escreva o código CSS real para estilizar seus componentes. Também remove o mapeamento entre componentes e estilos


