﻿# Como Iniciar o Cliente

## Requisitos

[![node](https://img.shields.io/badge/node-8.10.0-green.svg)](<[https://nodejs.org/en/](https://nodejs.org/en/)>)

[![yarn](https://img.shields.io/badge/yarn-1.16.0-blue.svg)](https://yarnpkg.com/pt-BR/docs/install#debian-stable)

## Baixar Projeto

`git clone https://gitlab.com/tetrinetjs/frontend.git`

## Iniciar server WebSocket

- Abra um terminal na pasta `/frontend/mockup`

- Execute os comandos :

`yarn install`

`yarn start`

- URL servidor: [ws://localhost:3030](ws://localhost:3030)

## Iniciar Cliente

- Abra outro terminal na pasta `/frontend/react-frontend`

- Execute os comandos :

`yarn install`

`yarn start`

- Abra o navegador na pagina: [http://localhost:3000](https://drive.google.com/file/d/1O_55Xvdbc0lqckbEGrFKkBm2comeKHGQ/view?usp=sharing)
