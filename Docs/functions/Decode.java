import java.io.*;
import java.util.*;


public class Encode 
{
   public static void main(String[] main){
	byte[] arrayb = {0,1,0,1};
		System.out.println("Encode: "+ encode("josé", "1.0", arrayb, false));
	}

    /**
     * Return the initialization string for the specified user.
     *
     * @param nickname  the nickname of the client
     * @param version   the version of the client
     * @param ip        the IP of the server
     * @param tetrifast is this a tetrifast client ?
     */
    public static String encode(String nickname, String version, byte[] ip, boolean tetrifast)
    {
        // compute the pattern
        int p = 54 * ip[0] + 41 * ip[1] + 29 * ip[2] + 17 * ip[3];
        char[] pattern = String.valueOf(p).toCharArray();

        // build the string to encode
        char[] data = ((tetrifast ? "tetrifaster " : "tetrisstart ") + nickname + " " + version).toCharArray();

        // build the encoded string
        StringBuffer result = new StringBuffer();
        char offset = 0x80;
        result.append(toHex(offset));

        char previous = offset;

        for (int i = 0; i < data.length; i++)
        {
            char current = (char) (((previous + data[i]) % 255) ^ pattern[i % pattern.length]);
            result.append(toHex(current));
            previous = current;
        }

        return result.toString().toUpperCase();
    }

    /**
     * Return the hex value of the specified byte on 2 digits.
     */
    private static String toHex(char c)
    {
        String h = Integer.toHexString(c);

        return h.length() > 1 ? h : "0" + h;
    }

    /**
     * Read a line as defined in the TetriNET protocol (that's ending with a
     * 0xFF character). 0xOA and 0xOD are also accepted as EOL characters.
     *
     * @since 0.2.1
     *
     * @param in the stream to be read
     * @throws IOException thrown if the stream is closed
     */
    public static String readLine(Reader in) throws IOException
    {
        StringBuffer input = new StringBuffer();

        int readChar;
        while ((readChar = in.read()) != -1 && readChar != 0xFF && readChar != 0x0A && readChar != 0x0D)
        {
            if (readChar != 0x0A && readChar != 0x0D)
            {
                input.append((char) readChar);
            }
        }

        if (readChar == -1)
        {
            throw new IOException("End of stream");
        }

        return input.toString();
    }

    public String toString()
    {
        return "[Protocol name=" + getName() + "]";
    }
    /**
     * Return the name of this protocol
     */
    public String getName()
    {
        return "tetrinet";
    }

}
