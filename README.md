﻿

# Equipe Interface (FrontEnd)

## Frontend React:
* DEMO: [frontend Tetrinet](https://tetrinetjs.gitlab.io/frontend/)

 * Iniciar Cliente: [README.md](https://gitlab.com/tetrinetjs/frontend/tree/master/Docs/Documenta%C3%A7%C3%A3o/Iniciar%20Cliente)
 * Dependências Usadas: [README.md](https://gitlab.com/tetrinetjs/frontend/tree/master/Docs/Documenta%C3%A7%C3%A3o/Depend%C3%AAncias%20usadas%20no%20cliente)
 * Funcionalidades Principais: [README.md](https://gitlab.com/tetrinetjs/frontend/tree/master/Docs/Documenta%C3%A7%C3%A3o/Funcionalidades%20Cliente)
 * Requisitos Futuros: [README.md](https://gitlab.com/tetrinetjs/frontend/tree/master/Docs/Documenta%C3%A7%C3%A3o/Requisitos%20futuros)
 * Mensagens trocadas: [Mensagens Trocadas](https://gitlab.com/tetrinetjs/frontend/raw/master/Docs/Mensagens.jpeg)
 * Interpretação da String: [README.md](https://gitlab.com/tetrinetjs/frontend/tree/master/Docs/Documenta%C3%A7%C3%A3o/Interpreta%C3%A7%C3%A3o%20Strings) [Tetrinet Protocol](https://gitlab.com/tetrinetjs/devel-docs/blob/jonathas/Files/tetrinetProtocol.pdf) [README.md](https://gitlab.com/tetrinetjs/devel-docs/blob/jonathas/Pages/31-PosicionarPeca.md) 

## Objetivos:

* Criar Interface baseada no jogo [blockbattle.net](http://blockbattle.net/)

## Grupo 8 - membros
* JADER HENRIQUE FARIA AMORIM (110992) (Líder)
* FELIPE GONÇALVES FERREIRA
* JONATHAS ASSUNCAO ALVES
* JOSUE GOMES QUEIROZ
* ODAIR XAVIER DA SILVA

## Tarefas da sprint 1 (29/04 a 05/05): 
* Definir sprints;
* Definir Framework a ser utilizado;
* Levantamento de Requisitos de interface gráfica do [blockbattle.net](http://blockbattle.net/);

## Tarefas da sprint 2 (06/05 a 12/05): 
* Documentar todas as peças do jogo (Formas e movimentações);
* Documentar todos os comandos da interface gráfica;
* Documentar todas as trilhas sonoras;
* Documentar todos os especiais (Tipo e ação);
* Codificar interface (Adicionar comandos básicos);
 
## Tarefas da sprint 3 (13/05 a 19/05):  
* Codificar interface (Adicionar sons);
* Codificar interface (Adicionar especiais);

## Tarefas da sprint 4 (20/05 a 26/05): 
* Versão final da interface (Testar funcionalidades);
* Refinar toda documentação, e fazer o branch para master;

## Guias e referências
* [Blockbattle](http://blockbattle.net/)
* [Brandly - react-tetris](https://github.com/brandly/react-tetris)
* [Chvin - react-tetris](https://github.com/chvin/react-tetris)
* [Gtetrinet](http://gtetrinet.sourceforge.net/)
* [Devel-Docs](https://gitlab.com/tetrinetjs/devel-docs/tree/jonathas/Pages)
* [Tetrinet Protocol](https://gitlab.com/tetrinetjs/devel-docs/blob/jonathas/Files/tetrinetProtocol.pdf)