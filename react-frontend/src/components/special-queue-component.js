import React, { Component } from "react";
import _TetrisPanel from "../style/_TetrisPanel";
import Constants from "../utils/Constants";
import EventEmitter from "../utils/event-emitter";

const {
  DivTetriMainStyle,
  DivTetriStyle,
  TdQueueStyle,
  TdDisablesStyle
} = _TetrisPanel;
const { dimension, events } = Constants;

class SpecialQueueComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      player: props.player,
      board: null,
      gamePanel: []
    };
  }

  componentDidMount() {
    EventEmitter.addListener(events.SPECIAL, value => {
      this.updateBoard(value);
    });
  }

  updateBoard(board) {
    this.setState({ board }, () => {
      this.converterString(this.state.board);
    });
  }

  converterString(string) {
    const gamePanel = [];

    for (var y = 0; y < dimension.QUEUE_SPECIAL; y++) {
      const classString = ` game-block piece-${string.charAt(y)}`;
      if (!!y) {
        gamePanel.push(<TdDisablesStyle key={y} className={classString} />);
      } else {
        gamePanel.push(<TdQueueStyle key={y} className={classString} />);
      }
    }

    this.setState({ gamePanel });
  }

  render() {
    return (
      <DivTetriMainStyle>
        <DivTetriStyle>
          <table>
            <tbody>
              <tr>{this.state.gamePanel}</tr>
            </tbody>
          </table>
        </DivTetriStyle>
      </DivTetriMainStyle>
    );
  }
}

export default SpecialQueueComponent;
