import React, { Component } from "react";
import _ChatPanel from "../style/_ChatPanel";
import EventEmitter from "../utils/event-emitter";
import restService from "../services/rest-service";
import Constants from "../utils/Constants";
import socketService from "../services/socket-service";

const {
  MesgsStyle,
  MsgHistoryStyle,
  TypeMsgStyle,
  WriteMsgStyle,
  MsgSendBtnStyle,
  IncomingMsgImgStyle,
  ImgStyle,
  ReceivedMsgStyle,
  ReceivedWithdMsgStyle,
  PrStyle,
  OutgoingMsgStyle,
  SentMsgStyle,
  PsStyle
} = _ChatPanel;
const { events } = Constants;

function showSender(id) {
  return restService().getPlayer(id).nickname;
}

function renderChat(idPlayer, msg, from, message) {
  let newMsg = null;
  if (restService().getPlayer(idPlayer).id === from) {
    newMsg = (
      <OutgoingMsgStyle key={from}>
        <SentMsgStyle>
          <PsStyle>{message}</PsStyle>
        </SentMsgStyle>
      </OutgoingMsgStyle>
    );
  } else {
    newMsg = (
      <div key={from}>
        <IncomingMsgImgStyle>
          <ImgStyle
            src="https://ptetutorials.com/images/user-profile.png"
            alt="sunil"
          />
        </IncomingMsgImgStyle>
        <ReceivedMsgStyle>
          <ReceivedWithdMsgStyle>
            <b>{showSender(from)}</b> enviou:
            <PrStyle>{message}</PrStyle>
          </ReceivedWithdMsgStyle>
        </ReceivedMsgStyle>
      </div>
    );
  }
  return [msg, newMsg];
}

class ChatComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      player: props.player,
      chat: null,
      text: ""
    };
    this.sendMessage = this.sendMessage.bind(this);
  }

  componentWillMount() {
    EventEmitter.addListener(events.CHAT, (player, message) => {
      console.log("Receive event 'events.CHAT'");
      this.setState(({ chat }) => {
        return {
          chat: renderChat(this.state.player, chat, player, message)
        };
      });
    });
  }

  sendMessage() {
    const msg = `pline ${restService().getPlayer(this.state.player).id} ${
      this.state.text
    }`;
    socketService().sendMSG(msg);
    this.setState({
      text: ""
    });
  }

  render() {
    return (
      <MesgsStyle>
        <MsgHistoryStyle>{this.state.chat}</MsgHistoryStyle>
        <TypeMsgStyle>
          <div>
            <WriteMsgStyle
              type="text"
              placeholder="Type a message"
              value={this.state.text}
              onChange={e => this.setState({ text: e.target.value })}
            />
            <MsgSendBtnStyle type="button" onClick={this.sendMessage}>
              <i className="fa fa-paper-plane-o" aria-hidden="true" />
            </MsgSendBtnStyle>
          </div>
        </TypeMsgStyle>
      </MesgsStyle>
    );
  }
}

export default ChatComponent;
