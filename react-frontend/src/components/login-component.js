import React, { Component } from "react";
import logo from "../img/logo.png";
import _LoginPanel from "../style/_LoginPanel";
import restService from "../services/rest-service";
import EventEmitter from "../utils/event-emitter";
import Constants from "../utils/Constants";

const { events } = Constants;
const {
  DivLoginStyle,
  DivPanelStyle,
  DivCardStyle,
  DivLogoStyle,
  DivLogoContainerStyle,
  ImgStyle,
  FormMainStyle,
  DivGroupStyle
} = _LoginPanel;

class LoginComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      nickname: null,
      team: null
    };
  }

  signOn() {
    if (!this.state.nickname) {
      alert("Informe seu nickname");
      return;
    }

    restService()
      .singOn(this.state.nickname, this.state.team)
      .then(a => {
        EventEmitter.emit(events.LOGIN, a);
      })
      .catch(err => {
        alert("Erro ao Logar: " + err);
      });
  }

  render() {
    return (
      <DivLoginStyle>
        <DivPanelStyle>
          <div className="col-sm-9 col-md-7 col-lg-5 mx-auto">
            <DivLogoStyle>
              <DivLogoContainerStyle>
                <DivGroupStyle>
                  <DivCardStyle>
                    <ImgStyle src={logo} alt="Logo" />
                  </DivCardStyle>
                </DivGroupStyle>
                <FormMainStyle>
                  <div className="form-group">
                    <label htmlFor="exampleInputEmail1">Nickname*</label>
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Informe o nickname*"
                      required
                      onInput={e => this.setState({ nickname: e.target.value })}
                    />
                    <small className="form-text text-muted">
                      Insira o seu nickname*.
                    </small>
                  </div>

                  <div className="form-group">
                    <label htmlFor="exampleInputEmail1">Time</label>
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Informe o time"
                      onInput={e => this.setState({ team: e.target.value })}
                    />
                    <small className="form-text text-muted">
                      Informe seu time.
                    </small>
                  </div>

                  <button
                    className="btn btn-lg btn-primary btn-block text-uppercase"
                    type="button"
                    onClick={this.signOn.bind(this)}
                  >
                    Entrar
                  </button>
                </FormMainStyle>
              </DivLogoContainerStyle>
            </DivLogoStyle>
          </div>
        </DivPanelStyle>
      </DivLoginStyle>
    );
  }
}

export default LoginComponent;
