import React, { Component } from "react";
import PieceQueueComponent from "../components/piece-queue-component";
import SpecialQueueComponent from "../components/special-queue-component";
import _TetrisPanel from "../style/_TetrisPanel";
import Constants from "../utils/Constants";
import EventEmitter from "../utils/event-emitter";
import UTILS from "../utils/func-utils";
import keyEvents from "../utils/key-events";
import restService from "../services/rest-service";

const { dimension, events } = Constants;
const { DivTetriStyle, DivTetriMainStyle, SpanStyle } = _TetrisPanel;

class TetrisPanelComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      idComponent: props.id,
      player: restService().getPlayer(props.id),
      board: null,
      gamePanel: [],
      tetrisMain: props.player
    };
  }

  componentDidMount() {
    this.updateBoard(this.startBoard());

    if (this.state.tetrisMain) {
      keyEvents().setIdPlayer(this.state.idComponent);
      keyEvents().addListener();
      const player = restService().getPlayer(this.state.idComponent);

      this.setState({
        nicknamePlayer: player.nickname,
        teamPlayer: player.team
      });
    }

    EventEmitter.addListener(events.GAME, (id, value) => {
      if (this.state.idComponent === id) {
        if (value.length === dimension.GAME_HEIGHT * dimension.GAME_WIDTH) {
          this.updateBoard(value);
        } else {
          this.updatePartialBoard(value);
        }
      }
    });

    EventEmitter.addListener(events.TEAM, (id, value) => {
      if (this.state.idComponent === id) {
        restService().setPlayer(id, { team: value });
        this.setState({ player: restService().getPlayer(id) });
      }
    });

    EventEmitter.addListener(events.JOIN, (id, value) => {
      if (this.state.idComponent === id) {
        restService().setPlayer(id, { nickname: value });
        this.setState({ player: restService().getPlayer(id) });
      }
    });

    EventEmitter.addListener(events.LOST, id => {
      if (this.state.tetrisMain && this.state.idComponent === id) {
        alert(`Você perdeu o jogo!!!`);
      }
    });
  }

  componentWillUnmount() {
    if (this.state.tetrisMain) {
      keyEvents().removeListener();
    }
  }

  startBoard() {
    // const total = dimension.GAME_HEIGHT * dimension.GAME_WIDTH;
    // let boardClear = "";

    // for (var i = 0; i < total; i++) boardClear += "0";

    // return boardClear;
    return "0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000005000000000055000000000050000000000000000000000000000000000000004220a0020344422011222333111101";
  }

  updateBoard(board) {
    this.setState({ board }, () => {
      this.converterString(this.state.board);
    });
  }

  updatePartialBoard(partialBoard) {
    const piece = UTILS.extractPiece(partialBoard);
    const newBoard = UTILS.setPartialBoard(
      this.state.board,
      partialBoard.substr(1),
      piece
    );
    this.setState({ board: newBoard }, () => {
      this.converterString(this.state.board);
    });
  }

  converterString(string) {
    const gamePanel = [];
    const tetrisDimesion = this.state.tetrisMain
      ? "playerDimension"
      : "otherPlayersDimension";

    for (var i = 0; i < string.length; i += dimension.GAME_HEIGHT) {
      const row = string.substr(i, dimension.GAME_HEIGHT);
      const rowArray = [];
      for (var y = 0; y < dimension.GAME_HEIGHT; y++) {
        const classString = `${tetrisDimesion} game-block piece-${row.charAt(
          y
        )}`;
        rowArray.push(<td key={y} className={classString} />);
      }
      gamePanel.push(<tr key={i}>{rowArray}</tr>);
    }

    this.setState({ gamePanel });
  }

  render() {
    return (
      <div>
        {this.state.tetrisMain ? (
          <DivTetriMainStyle>
            <span>
              <b>{this.state.idComponent}</b> - {this.state.player.nickname}{" "}
              {this.state.player.team ? (
                <b>{"(" + this.state.player.team + ")"}</b>
              ) : (
                ""
              )}
            </span>
            <PieceQueueComponent player={this.state.idComponent} />
            <table>
              <tbody>{this.state.gamePanel}</tbody>
            </table>
            <SpecialQueueComponent player={this.state.idComponent} />
          </DivTetriMainStyle>
        ) : (
          <DivTetriStyle>
            <SpanStyle>
              <b>{this.state.idComponent}</b> - {this.state.player.nickname}{" "}
              {this.state.player.team ? (
                <b>{"(" + this.state.player.team + ")"}</b>
              ) : (
                ""
              )}
            </SpanStyle>
            <table>
              <tbody>{this.state.gamePanel}</tbody>
            </table>
          </DivTetriStyle>
        )}
      </div>
    );
  }
}

export default TetrisPanelComponent;
