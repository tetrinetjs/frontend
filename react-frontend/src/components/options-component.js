import React, { Component } from "react";
import restService from "../services/rest-service";
import socketService from "../services/socket-service";
import _OptionPanel from "../style/_OptionPanel";
import Constants from "../utils/Constants";
import EventEmitter from "../utils/event-emitter";

const { events } = Constants;
const { DivGroupOptionStyle } = _OptionPanel;
class OptionsComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      player: props.player,
      startGame: false,
      pauseGame: false
    };
    this.updateStart = this.updateStart.bind(this);
    this.updatePause = this.updatePause.bind(this);
    this.logoff = this.logoff.bind(this);
  }

  componentWillMount() {
    EventEmitter.addListener(events.STARTGAME, statusGame => {
      console.log("Receive event 'events.STARTGAME'", statusGame);
      this.setState({ startGame: statusGame, pauseGame: false });
    });

    EventEmitter.addListener(events.PAUSE, statusPause => {
      console.log("Receive event 'events.PAUSE'");
      this.setState({ pauseGame: statusPause });
    });
  }

  updateStart() {
    const msg = `startgame ${Number(!this.state.startGame)} ${
      restService().getPlayer(this.state.player).id
    }`;
    socketService().sendMSG(msg);
  }

  updatePause() {
    const msg = `pause ${Number(!this.state.pauseGame)} ${
      restService().getPlayer(this.state.player).id
    }`;
    socketService().sendMSG(msg);
  }

  logoff() {
    const msg = `playerleave ${restService().getPlayer(this.state.player).id}`;
    socketService().sendMSG(msg);
  }

  render() {
    return (
      <DivGroupOptionStyle
        className="btn-group"
        role="group"
        aria-label="Basic example"
      >
        <button
          type="button"
          className="btn btn-secondary"
          disabled={this.state.startGame}
          onClick={this.updateStart}
        >
          <i className="mr-3 fa fa-plus-circle" aria-hidden="true" />
          Novo Jogo
        </button>
        <button
          type="button"
          className="btn btn-secondary"
          disabled={!(this.state.startGame && !this.state.pauseGame)}
          onClick={this.updatePause}
        >
          <i className="mr-3 fa fa-pause-circle" aria-hidden="true" />
          Pausar Jogo
        </button>
        <button
          type="button"
          className="btn btn-secondary"
          disabled={!(this.state.startGame && this.state.pauseGame)}
          onClick={this.updatePause}
        >
          <i className="mr-3 fa fa-play-circle" aria-hidden="true" />
          Continuar Jogo
        </button>
        <button
          type="button"
          className="btn btn-secondary"
          disabled={!this.state.startGame}
          onClick={this.updateStart}
        >
          <i className="mr-3 fa fa-stop-circle" aria-hidden="true" />
          Finalizar Jogo
        </button>
        <button
          type="button"
          className="btn btn-secondary"
          onClick={this.logoff}
        >
          <i className="mr-3 fa fa-sign-out" aria-hidden="true" />
          Logoff
        </button>
      </DivGroupOptionStyle>
    );
  }
}

export default OptionsComponent;
