import React, { Component } from "react";
import Tab from "react-bootstrap/Tab";
import Tabs from "react-bootstrap/Tabs";
import ChatComponent from "./components/chat-component";
import LoginComponent from "./components/login-component";
import OptionsComponent from "./components/options-component";
import RankingComponent from "./components/ranking-component";
import TetrisPanelComponent from "./components/tetris-panel-component";
import socketService from "./services/socket-service";
import _TetrisPanel from "./style/_TetrisPanel";
import Constants from "./utils/Constants";
import EventEmitter from "./utils/event-emitter";
import restService from "./services/rest-service";

const { events, dimension } = Constants;

const { DivPanelStyle, DivBoardStyle, DivBoardTopStyle } = _TetrisPanel;
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      player: null
    };
  }

  listenerEvents() {
    EventEmitter.removeAllListeners();
    EventEmitter.setMaxListeners(20);
    EventEmitter.addListener(events.LOGIN, value => {
      this.setState({ player: value });
      EventEmitter.addListener(events.LEAVE, value => {
        if (this.state.player.id === value) {
          this.setState({ player: null });
          this.listenerEvents();
        }
      });

      EventEmitter.addListener(events.WON, id => {
        alert(
          `O Jogador ${restService().getPlayer(id).nickname} venceu o jogo!!!`
        );
      });
    });
  }

  componentDidMount() {
    socketService().startSocket();
    this.listenerEvents();
  }

  generateTetrisPanel() {
    let rows = [];
    for (let i = 1; i <= dimension.MAX_PLAYERS; i++) {
      rows.push(
        <DivBoardStyle key={i}>
          <TetrisPanelComponent id={i} player={this.state.player.id === i} />
        </DivBoardStyle>
      );
    }

    return rows;
  }

  render() {
    return (
      <div>
        {!this.state.player ? (
          <LoginComponent />
        ) : (
          <DivBoardTopStyle>
            <Tabs defaultActiveKey="GAME" transition={false} id="tabs">
              <Tab eventKey="GAME" title="Jogo">
                <div>
                  <OptionsComponent player={this.state.player.id} />
                </div>
                <DivPanelStyle>{this.generateTetrisPanel()}</DivPanelStyle>
              </Tab>
              <Tab eventKey="CHAT" title="Chat">
                <ChatComponent player={this.state.player.id} />
              </Tab>
              <Tab eventKey="RANKING" title="Ranking">
                <RankingComponent />
              </Tab>
            </Tabs>
          </DivBoardTopStyle>
        )}
      </div>
    );
  }
}

export default App;
