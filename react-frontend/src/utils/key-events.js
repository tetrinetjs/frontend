import key from "keymaster";
import Constants from "./Constants";
import socketService from "../services/socket-service";
import EventEmitter from "../utils/event-emitter";

const { dimension, actions, events } = Constants;

const keyboardMap = {
  down: actions.MOVE_DOWN,
  left: actions.MOVE_LEFT,
  right: actions.MOVE_RIGHT,
  space: actions.HARD_DROP,
  up: actions.FLIP_CLOCKWISE,
  p: actions.PAUSE,
  d: actions.DROP_SPECIAL
};
const special = ["a", "b", "c", "g", "n", "o", "q", "r", "s"];

const player = {
  id: null,
  specialQueue: "an00000000"
};

function genId() {
  return Math.floor(Math.random() * (7 - 1)) + 1;
}

function genboard() {
  const total = dimension.GAME_HEIGHT * dimension.GAME_WIDTH;
  let board = "";

  for (var i = 0; i < total; i++) {
    const type = Math.floor(Math.random() * (1 - 10)) + 10;

    if (type > 8) {
      board += special[Math.floor(Math.random() * (0 - 8)) + 8];
    } else {
      board += Math.floor(Math.random() * (1 - 10)) + 10;
    }
  }

  return board;
}

function genSpecial() {
  const total = dimension.QUEUE_SPECIAL;
  let board = "";

  for (var i = 0; i < total; i++) {
    board += special[Math.floor(Math.random() * (0 - 8)) + 8];
  }

  return board;
}

function genPreview() {
  const total = dimension.QUEUE_PREVIEW * dimension.QUEUE_PREVIEW;
  let board = "";

  for (var i = 0; i < total; i++) {
    board += Math.floor(Math.random() * (1 - 10)) + 10;
  }

  return board;
}

function generateBoard(shortcut) {
  socketService().sendMSG(`f ${genId()} ${genboard()}`);
  socketService().sendMSG(`f ${genId()} ${genboard()}`);
  socketService().sendMSG(`f ${genId()} ${genboard()}`);
  socketService().sendMSG(`f ${genId()} ${genboard()}`);
  socketService().sendMSG(`f ${genId()} ${genboard()}`);
  socketService().sendMSG(`f ${genId()} ${genboard()}`);
  socketService().sendMSG(`fs ${player.id} ${genSpecial()}`);
  socketService().sendMSG(`fp ${player.id} ${genPreview()}`);
  socketService().sendMSG(`k ${player.id} ${shortcut}`);
}

function updateQueueSpecial() {
  player.specialQueue = player.specialQueue.substring(1) + "0";
  EventEmitter.emit(events.SPECIAL, player.specialQueue);
}

function keyEvents() {
  return {
    setIdPlayer: id => {
      player.id = id;
      updateQueueSpecial();
    },
    addListener: () => {
      EventEmitter.removeListener(events.QUEUE, () => {});
      EventEmitter.addListener(events.QUEUE, (id, value) => {
        if (player.id === id) {
          player.specialQueue = value;
          EventEmitter.emit(events.SPECIAL, player.specialQueue);
        }
      });

      Object.keys(keyboardMap).forEach(k => {
        key(k, (e, { shortcut }) => {
          generateBoard(shortcut);
        });
      });

      for (var i = 1; i <= dimension.MAX_PLAYERS; i++) {
        key(`${i}`, (e, { shortcut }) => {
          if (
            !!player.specialQueue &&
            player.specialQueue[0].match(/[a-z]/i) &&
            !!player.id
          ) {
            socketService().sendMSG(
              `sb ${shortcut} ${player.specialQueue[0]} ${player.id}`
            );
            updateQueueSpecial();
          }
        });
      }
    },
    removeListener: () => {
      Object.keys(keyboardMap).forEach(k => {
        key.unbind(k);
      });

      for (var i = 1; i <= dimension.MAX_PLAYERS; i++) {
        key.unbind(`${i}`);
      }
    }
  };
}

export default keyEvents;
