import Constants from "../utils/Constants";
import _ from "lodash";
const { dimension } = Constants;

const UTILS = {
  extractPiece(partialBoard) {
    console.log(partialBoard[0]);
    switch (partialBoard[0]) {
      case '"':
        return "1";
      case "#":
        return "2";
      case "$":
        return "3";
      case "%":
        return "4";
      case "&":
        return "5";
      case "'":
        return "a";
      case "(":
        return "c";
      case ")":
        return "n";
      case "*":
        return "r";
      case "+":
        return "s";
      case ",":
        return "b";
      case "-":
        return "g";
      case ".":
        return "q";
      case "/":
        return "o";
      default:
        return null;
    }
  },

  setPartialBoard(board, partialBoard, piece) {
    let newBoard = _.cloneDeep(board);
    let partialBoardConverter = _.cloneDeep(_.toUpper(_.trim(partialBoard)));

    for (var i = 0; i < partialBoardConverter.length; i += 2) {
      var coordinate =
        (partialBoardConverter[i + 1].charCodeAt(0) - 51) *
          dimension.GAME_HEIGHT +
        (partialBoardConverter[i].charCodeAt(0) - 51);
      newBoard =
        newBoard.substring(0, coordinate) +
        piece +
        newBoard.substring(coordinate + 1);
    }
    return newBoard;
  }
};

export default UTILS;
