import Constants from "../utils/Constants";
/**
 * @author Jônathas Assunção
 * @contact  jaa020399@gmail.com
 * @date 29/05/2019
 */
const { url } = Constants;
const PLAYER = [
  {
    id: 1,
    nickname: `Jogador 1`,
    team: ""
  },
  {
    id: 2,
    nickname: `Jogador 2`,
    team: ""
  },
  {
    id: 3,
    nickname: `Jogador 3`,
    team: ""
  },
  {
    id: 4,
    nickname: `Jogador 4`,
    team: ""
  },
  {
    id: 5,
    nickname: `Jogador 5`,
    team: ""
  },
  {
    id: 6,
    nickname: `Jogador 6`,
    team: ""
  }
];
export default function restService() {
  return {
    getPlayer: id => {
      return PLAYER[id - 1];
    },
    setPlayer: (id, { nickname, team }) => {
      if (!!nickname) {
        PLAYER[id - 1].nickname = nickname;
      }
      if (!!team) {
        PLAYER[id - 1].team = team;
      }
    },
    // Start new Game
    singOn: (nickname, team) => {
      console.log(`Requesting recourse in GET=>'${url.REST}/01001000/json/'`);
      return fetch(`${url.REST}/01001000/json/`)
        .then(res => {
          return res.json();
        })
        .then(data => {
          const id = Number(Math.floor(Math.random() * (6 - 1)) + 1);
          restService().setPlayer(id, { nickname, team });
          return restService().getPlayer(id);
        });
    },

    // Start new Game
    getRanking: () => {
      console.log(`Requesting recourse in GET=>'${url.REST}/01001000/json/'`);
      return fetch(`${url.REST}/01001000/json/`).then(res => {
        return res.json();
      });
    }
  };
}
