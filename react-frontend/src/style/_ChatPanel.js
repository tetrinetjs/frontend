import styled from "styled-components";

export default {
  MesgsStyle: styled.div`
    float: left;
    padding: 30px 15px 0 25px;
    width: 100%;
    background-color: #fff;
    border: 1px solid rgba(0, 0, 0, 0.125);
    border-radius: 0.25rem;
    border-color: #007bff !important;
  `,

  MsgHistoryStyle: styled.div`
    height: 516px;
    overflow-y: auto;
  `,

  TypeMsgStyle: styled.div`
    border-top: 1px solid #c4c4c4;
    position: relative;
  `,

  WriteMsgStyle: styled.input`
    background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
    border: medium none;
    color: #4c4c4c;
    font-size: 15px;
    min-height: 48px;
    width: 100%;
  `,

  MsgSendBtnStyle: styled.button`
    background: #05728f none repeat scroll 0 0;
    border: medium none;
    border-radius: 50%;
    color: #fff;
    cursor: pointer;
    font-size: 17px;
    height: 33px;
    position: absolute;
    right: 0;
    top: 11px;
    width: 33px;
  `,

  IncomingMsgImgStyle: styled.div`
    display: inline-block;
    width: 6%;
  `,

  ImgStyle: styled.img`
    vertical-align: middle;
    border-style: none;
    max-width: 100%;
  `,

  ReceivedMsgStyle: styled.div`
    display: inline-block;
    padding: 0 0 0 10px;
    vertical-align: top;
    width: 92%;
  `,

  ReceivedWithdMsgStyle: styled.div`
    width: 57%;
  `,

  PrStyle: styled.p`
    background: #ebebeb none repeat scroll 0 0;
    border-radius: 3px;
    color: #646464;
    font-size: 14px;
    margin: 0;
    padding: 5px 10px 5px 12px;
    width: 100%;
  `,

  OutgoingMsgStyle: styled.div`
    overflow: hidden;
    margin: 26px 0 26px;
  `,

  SentMsgStyle: styled.div`
    float: right;
    width: 46%;
  `,

  PsStyle: styled.p`
    background: #05728f none repeat scroll 0 0;
    border-radius: 3px;
    font-size: 14px;
    margin: 0;
    color: #fff;
    padding: 5px 10px 5px 12px;
    width: 100%;
  `
};
