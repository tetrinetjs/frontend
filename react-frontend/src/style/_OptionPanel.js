import styled from "styled-components";

export default {
  DivGroupOptionStyle: styled.div`
    display: grid !important;
    width: 100%;
    grid-template-columns: repeat(5, 15vw);
    justify-content: center;
    padding: 5px;
  `
};
